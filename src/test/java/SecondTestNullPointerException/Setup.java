package SecondTestNullPointerException;

import org.testng.annotations.BeforeSuite;

public class Setup {

	public static String someVariable;

	@BeforeSuite
	public void setupVariable() {
		System.out.println("Executing setupVariable...");
		someVariable = "someValue";
	}

}
