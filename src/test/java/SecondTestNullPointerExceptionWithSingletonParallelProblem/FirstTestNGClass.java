package SecondTestNullPointerExceptionWithSingletonParallelProblem;

import org.testng.annotations.Test;

public class FirstTestNGClass extends Setup{
	
	@Test
	public void firstMethodOfFirstTestNGClass()
	{
		System.out.println("Executing firstMethodOfFirstTestNGClass by Thread "+ Thread.currentThread().getId());
		System.out.println("Value of someVariable is : "+ sharedVariables.getSomeVariable());
		System.out.println("Value of name is :"+ sharedVariablesMap.get("Name"));
	}
	
	@Test
	public void secondMethodOfFirstTestNGClass()
	{
		System.out.println("Executing secondMethodOfFirstTestNGClass by Thread " + Thread.currentThread().getId());
		System.out.println("Value of someVariable is : "+ sharedVariables.getSomeVariable());
		System.out.println("Value of name is :"+ sharedVariablesMap.get("Name"));
	}
}
