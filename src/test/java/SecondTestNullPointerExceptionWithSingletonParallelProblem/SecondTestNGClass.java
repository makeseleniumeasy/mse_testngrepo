package SecondTestNullPointerExceptionWithSingletonParallelProblem;

import org.testng.annotations.Test;

public class SecondTestNGClass extends Setup{
	
	@Test
	public void firstMethodOfSecondTestNGClass()
	{
		System.out.println("Executing firstMethodOfSecondTestNGClass by Thread "+Thread.currentThread().getId());
		System.out.println("Value of someVariable is : "+ sharedVariables.getSomeVariable());
		System.out.println("Value of name is :"+ sharedVariablesMap.get("Name"));
	}
	
	@Test
	public void secondMethodOfSecondTestNGClass()
	{
		System.out.println("Executing secondMethodOfSecondTestNGClass by Thread "+ Thread.currentThread().getId());
		System.out.println("Value of someVariable is : "+ sharedVariables.getSomeVariable());
		System.out.println("Value of name is :"+ sharedVariablesMap.get("Name"));
	}
}
