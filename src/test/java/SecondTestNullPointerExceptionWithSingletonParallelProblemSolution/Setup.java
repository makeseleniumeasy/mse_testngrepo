package SecondTestNullPointerExceptionWithSingletonParallelProblemSolution;

import java.util.Random;

import org.testng.annotations.BeforeMethod;

public class Setup {

	// Get same instance of SharedVariables always
	SharedVariables sharedVariables;
	SharedVariablesMap sharedVariablesMap;

	@BeforeMethod
	public void setupVariable() throws InterruptedException {
		sharedVariables = SharedVariables.getInstance();
		sharedVariablesMap = SharedVariablesMap.getInstance();
		System.out.println("Executing setupVariable By Thread "+Thread.currentThread().getId());
		String somevalue = "someValue" + new Random().nextInt();
		String name = "Amod" + new Random().nextInt();
		System.out.println("Value set for SomeVariable by Thread "+Thread.currentThread().getId() + " as " + somevalue);
		System.out.println("Value set for Name by Thread "+Thread.currentThread().getId() + " as "+name);
		// Making thread sleep
		int waitTime = new Random().nextInt((9 - 1) + 1) + 1;
		System.out.println("Waiting for : "+waitTime+" sec");
		Thread.sleep(waitTime*1000);
		sharedVariables.setSomeVariable(somevalue);
		sharedVariablesMap.store("Name", name);
	}

}
