package SecondTestNullPointerExceptionWithSingletonParallelProblemSolution;

public class SharedVariables {

	// Private instance of class
	private static final ThreadLocal<SharedVariables> instance = new ThreadLocal<SharedVariables>();

	// Private constructor to control object creation of class
	private SharedVariables() {
	}

	// Return unidealized instance of class 
	public static SharedVariables getInstance() {
		if(instance.get() == null)
			instance.set(new SharedVariables());
		return instance.get();
	}

	// A private variable with getter and setter methods
	private String someVariable;

	public String getSomeVariable() {
		return someVariable;
	}

	public void setSomeVariable(String someVariable) {
		this.someVariable = someVariable;
	}
}
