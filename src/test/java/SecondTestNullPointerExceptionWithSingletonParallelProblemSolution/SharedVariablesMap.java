package SecondTestNullPointerExceptionWithSingletonParallelProblemSolution;

import java.util.HashMap;

public class SharedVariablesMap {

	// Private instance of class
	private static final ThreadLocal<SharedVariablesMap> instance = new ThreadLocal<SharedVariablesMap>();
	
	private static final ThreadLocal<HashMap<String,Object>> dataStore = new ThreadLocal<HashMap<String,Object>>();
	
	// Private constructor to control object creation of class
	private SharedVariablesMap() {
	}

	// Return unidealized instance of class 
	public static SharedVariablesMap getInstance() {
		if(instance.get() == null)
		{
			instance.set(new SharedVariablesMap());
			dataStore.set(new HashMap<String,Object>());
		}
		return instance.get();
			
	}
	
	public void store(String key, String value)
	{
		dataStore.get().put(key, value);
	}
	
	public Object get(String key)
	{
		return dataStore.get().get(key);
	}
}
